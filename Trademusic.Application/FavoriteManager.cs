﻿using System.Linq;

using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Application
{
    public class FavoriteManager : Manager<Favorite> , IFavoriteManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public FavoriteManager(IApplicationDbContext _db)
            : base(_db)
        {
        }

        /// <summary>
        /// Método que devuelve todos los favoritos de un usuario 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IQueryable<Favorite> GetAllByUserId(string id)
        {
            return db.Favorites.Where(f => f.User.Id == id);
        }
    }
}