﻿using System.Linq;
using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Application
{
    public class FamilyManager : Manager<Family> , IFamilyManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public FamilyManager(IApplicationDbContext _db)
            : base(_db)
        {
        }

        /// <summary>
        /// Método que devuelve la primera familia de un padre en concreto
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public Family GetByParentId(int parentId)
        {
            return db.Families.FirstOrDefault(f => f.ParentId == parentId);
        }

        /// <summary>
        /// Método que devuelve todas las familias con un padre en concreto
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public IQueryable<Family> GetAllByParentId(int parentId)
        {
            return db.Families.Where(f => f.ParentId == parentId);
        }
    }
}