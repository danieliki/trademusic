﻿using System.Linq;
using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Application
{
    public class UserManager : Manager<ApplicationUser>, IUserManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public UserManager(IApplicationDbContext _db)
            : base(_db)
        { }

        /// <summary>
        /// Método que devuelve el usuario por sus datos de login 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="pass"></param>
        /// <returns></returns>
        public ApplicationUser GetLoginUser(string email, string pass)
        {
            return db.Users.SingleOrDefault(u => u.Email == email && u.Password == pass);
        }
    }
}