﻿using System;
using System.Linq;

using Trademusic.CORE.Contracts;

namespace Trademusic.Application
{
    /// <summary>
    /// Manager genérico
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Manager<T> : IManager<T>
        where T : class
    {

        public IApplicationDbContext db {get;}

        /// <summary>
        /// Constructor del manager generico
        /// </summary>
        /// <param name="db"></param>
        public Manager(IApplicationDbContext _db)
        {
            db = _db;
        }


        /// <summary>
        /// Método que devuelve todos los resultados de una clase
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> GetAll(string include = null)
        {
            if (include == null)
            {
                return db.Set<T>();
            }
            return db.Set<T>().Include(include);
        }

        /// <summary>
        /// Método que añade un registro a una clase
        /// </summary>
        /// <param name="entity"></param>
        public T Add(T entity)
        {
            return db.Set<T>().Add(entity);
        }

        /// <summary>
        /// Método que devuelve un registro por su id
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetById(object[] key)
        {
            return db.Set<T>().Find(key);
        }

        /// <summary>
        /// Método que devuelve un registro por su id
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetById(int key)
        {
            return db.Set<T>().Find(key);
        }

        /// <summary>
        /// Método que devuelve un registro por su id
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetById(Guid key)
        {
            return db.Set<T>().Find(key);
        }

        /// <summary>
        /// Método que devuelve un registro por su id
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetById(string key)
        {
            return db.Set<T>().Find(key);
        }

        /// <summary>
        /// Método que devuelve un registro por su nombre
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public T GetByName(string name)
        {
            return db.Set<T>().Find(name);
        }

        /// <summary>
        /// Método que elimina un registro
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public T Delete(T entity)
        {
            return db.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Método de persistencia para guardar los cambios
        /// </summary>
        /// <returns></returns>
        public int SaveChanges()
        {
            return db.SaveChanges();
        }

        /// <summary>
        /// Método que modifica un registro
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="newData"></param>
        public void Edit(T entity, T newData)
        {
            db.Entry(entity).CurrentValues.SetValues(newData);
        }

        /// <summary>
        /// Método que modifica un registro
        /// </summary>
        /// <param name="key"></param>
        /// <param name="newData"></param>
        public void Edit(int key, T newData)
        {
            Edit(GetById(key), newData);
        }

        /// <summary>
        /// Método que modifica un registro
        /// </summary>
        /// <param name="key"></param>
        /// <param name="newData"></param>
        public void Edit(Guid key, T newData)
        {
            Edit(GetById(key), newData);
        }

        /// <summary>
        /// Método que modifica un registro
        /// </summary>
        /// <param name="key"></param>
        /// <param name="newData"></param>
        public void Edit(string name, T newData)
        {
            Edit(GetById(name), newData);
        }

        /// <summary>
        /// Método que modifica un registro
        /// </summary>
        /// <param name="key"></param>
        /// <param name="newData"></param>
        public void Edit(object [] key, T newData)
        {
            Edit(GetById(key), newData);
        }
    }
}