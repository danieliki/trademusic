﻿using System.Linq;
using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Application
{
    public class MessageManager : Manager<Message>, IMessageManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public MessageManager(IApplicationDbContext _db)
            : base(_db)
        {
        }

        /// <summary>
        /// Método que devuelve todos los mensajes de un instrumento
        /// </summary>
        /// <param name="instrumentId"></param>
        /// <returns></returns>
        public IQueryable<Message> GetAllByInstrumentId(int instrumentId)
        {
            return db.Messages.Where(c => c.InstrumentId == instrumentId);
        }

        /// <summary>
        /// Método que devuelve todos los mensajes del usuario al que se los han escrito
        /// </summary>
        /// <param name="instrumentId"></param>
        /// <returns></returns>
        public IQueryable<Message> GetAllByOwnerId(string ownerId)
        {
            return db.Messages.Where(c => c.OwnerId == ownerId);
        }

        /// <summary>
        /// Método que devuelve todos los mensajes del usuario al que se los han escrito y además no los haya leído
        /// </summary>
        /// <param name="instrumentId"></param>
        /// <returns></returns>
        public IQueryable<Message> GetAllByOwnerIdNoRead(string ownerId)
        {
            return db.Messages.Where(c => c.OwnerId == ownerId && c.Read == false);
        }
    }
}