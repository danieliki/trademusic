﻿using System.Linq;
using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Application
{
    public class InstrumentManager : Manager<Instrument>, IInstrumentManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public InstrumentManager(IApplicationDbContext _db)
            : base(_db)
        {
        }
       
        /// <summary>
        /// Método que devuelve todos los instrumentos por su marca 
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        public IQueryable<Instrument> GetAllByBrand(string brand)
        {
            return db.Instruments.Where(v => v.Brand == brand);
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos por su marca y familia
        /// </summary>
        /// <param name="family"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public IQueryable<Instrument> GetAllByFamilyAndBrand(int family, string brand)
        {
            return db.Instruments.Where(v => v.FamilyId == family && v.Brand == brand);
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos por su familia y ciudad
        /// </summary>
        /// <param name="family"></param>
        /// <param name="city"></param>
        /// <returns></returns>
        public IQueryable<Instrument> GetAllByFamilyAndCity(int family, string city)
        {
            return db.Instruments.Where(v => v.FamilyId == family && v.User.City == city);
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos por su familia y comprendidos dentro de un rango de precio 
        /// </summary>
        /// <param name="family"></param>
        /// <param name="price1"></param>
        /// <param name="price2"></param>
        /// <returns></returns>
        public IQueryable<Instrument> GetAllByFamilyAndPriceRange(int family, int price1, int price2)
        {
            return db.Instruments.Where(v => v.FamilyId == family && v.Price >= price1 && v.Price <= price2);
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos por el nombre de la familia a la que pertenecen
        /// </summary>
        /// <param name="familyId"></param>
        /// <returns></returns>
        public IQueryable<Instrument> GetAllByFamily(string familyName)
        {
            return db.Instruments.Where(v => v.Family.Name == familyName);
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos por el identificador de la familia a la que pertenecen
        /// </summary>
        /// <param name="familyId"></param>
        /// <returns></returns>
        public IQueryable<Instrument> GetAllByFamily(int familyId)
        {
            return db.Instruments.Where(v => v.FamilyId == familyId);
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos comprendidos en un rango de precios
        /// </summary>
        /// <param name="price1"></param>
        /// <param name="price2"></param>
        /// <returns></returns>
        public IQueryable<Instrument> GetAllByPriceRange(decimal price1, decimal price2)
        {
            return db.Instruments.Where(v => v.Price >= price1 && v.Price <= price2);
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos por la ciudad de su propietario
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        public IQueryable<Instrument> GetAllByUserCity(string city)
        {
            return db.Instruments.Where(v => v.User.City == city);
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos por su propietario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IQueryable<Instrument> GetAllByUserId(string id)
        {
            return db.Instruments.Where(v => v.UserId == id);
        }

        /// <summary>
        /// Método que devuelve un instrumento por su propietario 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Instrument GetByUserId(string id)
        {
            return db.Instruments.SingleOrDefault(v => v.UserId == id);
        }
    }
}