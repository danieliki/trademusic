﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Trademusic.CORE.Contracts;
using Trademusic.Web.Areas.Admin.Models.Instrument;
using Trademusic.Web.JQueryDataTable;

namespace Trademusic.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Controlador de los instrumentos - Solo accesible con el rol Admin
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class InstrumentController : Controller
    {
        private IInstrumentManager instrumentManager;
        private IFamilyManager familyManager;
        private IUserManager userManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="instrumentManager"></param>
        /// <param name="familyManager"></param>
        public InstrumentController(IInstrumentManager instrumentManager, IFamilyManager familyManager, IUserManager userManager)
        {
            this.instrumentManager = instrumentManager;
            this.familyManager = familyManager;
            this.userManager = userManager;
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexData()
        {
            JQueryDataTableRequest request = new JQueryDataTableRequest(System.Web.HttpContext.Current.Request);
            var model =
                instrumentManager.GetAll("Family")
                    .Select(e => new InstrumentIndexViewModel {
                                Id = e.Id,
                                Brand = e.Brand,
                                Model = e.Model,
                                Price = e.Price,
                                User = e.User.Email
                            });

            JQueryDataTableResponse result = JQueryHelper.CreateJQueryDataTableResponse(model, request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que muestra una tabla con todos los instrumentos
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model =
                instrumentManager.GetAll("Family")
                    .Select(e => new InstrumentIndexViewModel {
                        Id = e.Id,
                        Brand = e.Brand,
                        Model = e.Model,
                        Price = e.Price,
                        User = e.User.Email
                    });

            return View(model);
        }

        /// <summary>
        /// Método que pinta la vista de edición de un instrumento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            InstrumentCreateViewModel model = new InstrumentCreateViewModel();
            model = PrepareViewModel(model);
            model.Instrument = instrumentManager.GetById(id);
            model.Instrument.User = userManager.GetById(model.Instrument.UserId);
            model.ImagesFiles = new List<string>();

            var referenceUri = new Uri(@Server.MapPath("~\\Images\\Instrument\\"));

            foreach (var file in Directory.GetFiles(@Server.MapPath("~\\Images\\Instruments\\"), id + "_*"))
            {
                var fileUri = new Uri(file);
                model.ImagesFiles.Add("/Images/Instrument/" + referenceUri.MakeRelativeUri(fileUri));
            }
            return View(model);       
        }

        /// <summary>
        /// Método de edición de un instrumento
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(int id, InstrumentCreateViewModel model)
        {
            var instrument = instrumentManager.GetById(id);
            model.Instrument.UserId = instrument.User.Id;
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Images != null)
                    {
                        foreach (var image in model.Images)
                        {
                            if (image != null)
                            {
                                image.SaveAs(Path.Combine(@Server.MapPath("~\\Images\\Instruments\\"), id + "_" + model.Images.IndexOf(image) + ".jpg"));
                            }
                        }
                    }
                
                    instrumentManager.Edit(id, model.Instrument);
                    instrumentManager.db.SaveChanges();
                    model = PrepareViewModel(model);
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch(Exception e)
            {
                model = PrepareViewModel(model);
                return View(model);
            }
        }

        /// <summary>
        /// Método que prepara los dropdownlist de la vista
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private InstrumentCreateViewModel PrepareViewModel(InstrumentCreateViewModel model)
        {
            model.Family =
                familyManager.GetAll()
                    .Select(f => new SelectListItem { Value = f.Id.ToString(), Text = f.Name })
                    .ToList();
            return model;
        } 
    }
}