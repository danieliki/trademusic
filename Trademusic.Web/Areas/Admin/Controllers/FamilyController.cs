﻿using System;
using System.Linq;
using System.Web.Mvc;
using Trademusic.CORE.Contracts;
using Trademusic.Web.Areas.Admin.Models.Family;
using Trademusic.Web.JQueryDataTable;

namespace Trademusic.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Controlador de las familias - Solo accesible con el rol Admin
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class FamilyController : Controller
    {
        private IFamilyManager familyManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="familyManager"></param>
        public FamilyController(IFamilyManager familyManager)
        {
            this.familyManager = familyManager;
        }

        /// <summary>
        /// Método que devuelve todas las familias en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexData()
        {
            JQueryDataTableRequest request = new JQueryDataTableRequest(System.Web.HttpContext.Current.Request);
            var model =
                familyManager.GetAll()
                    .Select(e => new FamilyIndexViewModel {
                        Id = e.Id,
                        Name = e.Name,
                        Parent = e.Parent.Name,
                        ParentId = e.ParentId,
                        EnglishName = e.EnglishName
                    });

            JQueryDataTableResponse result = JQueryHelper.CreateJQueryDataTableResponse(model, request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que muestra una tabla con todas las familias
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model =
               familyManager.GetAll()
                   .Select(e => new FamilyIndexViewModel {
                       Id = e.Id,
                       Name = e.Name,
                       Parent = e.Parent.Name,
                       ParentId = e.ParentId,
                       EnglishName = e.EnglishName
                   });

            return View(model);
        }

        /// <summary>
        /// Método que pinta la vista de creación de una familia
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            FamilyCreateViewModel model = new FamilyCreateViewModel();
            model = PrepareViewModel(model);

            return View(model);
        }

        /// <summary>
        /// Método que crea una familia
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(FamilyCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    familyManager.Add(model.Family);
                    familyManager.db.SaveChanges();
                    return RedirectToAction("Index");
                }
                model = PrepareViewModel(model);
                return View(model);
            }
            catch (Exception e)
            {
                return View("Error" + e);
            }
        }

        /// <summary>
        /// Método que pinta la vista de edición de una familia
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            FamilyCreateViewModel model = new FamilyCreateViewModel();
            model = PrepareViewModel(model);
            model.Family = familyManager.GetById(id);

            return View(model);
        }

        /// <summary>
        /// Método de edición de una familia
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(int id, FamilyCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    familyManager.Edit(id, model.Family);
                    familyManager.db.SaveChanges();
                    return RedirectToAction("Index");
                }
                model = PrepareViewModel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        /// <summary>
        /// Método que prepara los dropdownlist de la vista
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private FamilyCreateViewModel PrepareViewModel(FamilyCreateViewModel model)
        {
            model.Parent =
                familyManager.GetAll()
                    .Select(f => new SelectListItem { Value = f.Id.ToString(), Text = f.Name })
                    .ToList();
            model.Parent.Insert(
                0,
                new SelectListItem { Value = "0", Text = "Seleccione una opción", Selected = true });
            return model;
        }
    }
}
