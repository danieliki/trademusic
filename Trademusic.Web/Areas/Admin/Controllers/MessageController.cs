﻿using System;
using System.Linq;
using System.Web.Mvc;
using Trademusic.CORE.Contracts;
using Trademusic.Web.Areas.Admin.Models.Message;
using Trademusic.Web.JQueryDataTable;

namespace Trademusic.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Controlador de los mensajes - Solo accesible con el rol Admin
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class MessageController : Controller
    {
        private IMessageManager messageManager;
        private IInstrumentManager instrumentManager;
        private IUserManager userManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="messageManager"></param>
        /// <param name="instrumentManager"></param>
        public MessageController(IMessageManager messageManager, IInstrumentManager instrumentManager, IUserManager userManager)
        {
            this.messageManager = messageManager;
            this.instrumentManager = instrumentManager;
            this.userManager = userManager;
        }

        /// <summary>
        /// Método que devuelve todos los mensajes en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexData()
        {
            JQueryDataTableRequest request = new JQueryDataTableRequest(System.Web.HttpContext.Current.Request);
            var model =
                messageManager.GetAll()
                    .Select(e => new MessageIndexViewModel {
                        Id = e.Id,
                        Instrument = e.Instrument.Brand + "-" + e.Instrument.Model,
                        UserEmail = e.User.Email,
                        Date = e.DateTime,
                        Text = e.Text
                    });

            JQueryDataTableResponse result = JQueryHelper.CreateJQueryDataTableResponse(model, request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que muestra una tabla con todos los mensajes
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model =
                messageManager.GetAll()
                    .Select(e => new MessageIndexViewModel {
                        Id = e.Id,
                        Instrument = e.Instrument.Brand + "-" + e.Instrument.Model,
                        UserEmail = e.User.Email,
                        Date = e.DateTime,
                        Text = e.Text
                    });

            return View(model);
        }

        /// <summary>
        /// Método que pinta la vista de edición de un mensaje
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            MessageCreateViewModel model = new MessageCreateViewModel();
            model = PrepareViewModel(model);
            model.Message = messageManager.GetById(id);
            model.Message.User = userManager.GetById(model.Message.UserId);

            return View(model);
        }

        /// <summary>
        /// Método de edición de un mensaje
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(int id, MessageCreateViewModel model)
        {
            var message = messageManager.GetById(id);
            model.Message.UserId = message.User.Id;
            try
            {
                if (ModelState.IsValid)
                {
                    messageManager.Edit(id, model.Message);
                    messageManager.db.SaveChanges();
                    return RedirectToAction("Index");
                }
                model = PrepareViewModel(model);
                return View(model);
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        /// <summary>
        /// Método que prepara los dropdownlist de la vista
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private MessageCreateViewModel PrepareViewModel(MessageCreateViewModel model)
        {
            model.Instrument =
                instrumentManager.GetAll()
                    .Select(p => new SelectListItem { Value = p.Id.ToString(), Text = p.Brand + p.Model })
                    .ToList();
            return model;
        } 
    }
}
