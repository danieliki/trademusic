﻿using System;
using System.Linq;
using System.Web.Mvc;
using Trademusic.CORE.Contracts;
using Trademusic.Web.Areas.Admin.Models.User;
using Trademusic.Web.JQueryDataTable;

namespace Trademusic.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Controlador de los usuarios - Solo accesible con el rol Admin
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private IUserManager userManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="userManager"></param>
        public UserController(IUserManager userManager)
        {
            this.userManager = userManager;
        }

        /// <summary>
        /// Método que devuelve todos los usuarios en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexData()
        {
            JQueryDataTableRequest request = new JQueryDataTableRequest(System.Web.HttpContext.Current.Request);
            var model =
                userManager.GetAll()
                    .Select(e => new UserIndexViewModel
                    {
                        Id = e.Id,
                        Name = e.Name,
                        SurName1 = e.SurName1,
                        SurName2 = e.SurName2,
                        Address = e.Address,
                        ZipCode = e.ZipCode,
                        City = e.City,
                        Email = e.Email,
                        Password = e.Password,
                        Latitude = e.Latitude,
                        Longitude = e.Longitude
                    });

            JQueryDataTableResponse result = JQueryHelper.CreateJQueryDataTableResponse(model, request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que muestra una tabla con todos los usuarios
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model =
                userManager.GetAll()
                    .Select(e => new UserIndexViewModel
                    {
                        Id = e.Id,
                        Name = e.Name,
                        SurName1 = e.SurName1,
                        SurName2 = e.SurName2,
                        Address = e.Address,
                        ZipCode = e.ZipCode,
                        City = e.City,
                        Email = e.Email,
                        Password = e.Password,
                        Latitude = e.Latitude,
                        Longitude = e.Longitude
                    });

            return View(model);
        }
    }
}