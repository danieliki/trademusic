﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Trademusic.Web.Areas.Admin.Models.Family
{
    public class FamilyCreateViewModel
    {

        public CORE.Domain.Classes.Family Family { get; set; }

        public List<SelectListItem> Parent { get; set; }
    }
}