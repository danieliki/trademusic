﻿
namespace Trademusic.Web.Areas.Admin.Models.Family
{
    public class FamilyIndexViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Parent { get; set; }

        public int? ParentId { get; set; }

        public string EnglishName { get; set; }
    }
}