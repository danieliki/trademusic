﻿
namespace Trademusic.Web.Areas.Admin.Models.User
{
    public class UserCreateViewModel
    {
        public CORE.Domain.Classes.ApplicationUser User { get; set; }
    }
}