﻿namespace Trademusic.Web.Areas.Admin.Models.User
{
    public class UserIndexViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
        
        public string SurName1 { get; set; }

        public string SurName2 { get; set; }

        public string Address { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Latitude { get; set; }
        
        public string Longitude { get; set; }
      
       
    }
}