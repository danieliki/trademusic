﻿using System.ComponentModel.DataAnnotations;

using Trademusic.Web.App_GlobalResources;

namespace Trademusic.Web.Areas.Admin.Models.Instrument
{

    /// <summary>
    /// ViewModel para mostrar el listado del instrumento de administración
    /// </summary>
    public class InstrumentIndexViewModel
    {
        public int Id { get; set; }
      
        [Display(ResourceType = typeof(Language), Name = "BRAND")]
        public string Brand { get; set; }

        [Display(ResourceType = typeof(Language), Name = "MODEL")]
        public string Model { get; set; }

        [Display(ResourceType = typeof(Language), Name = "PRICE")]
        [DisplayFormat(DataFormatString = "{0:0,00}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        [Display(ResourceType = typeof(Language), Name = "USER")]
        public string User { get; set; }
    }
}