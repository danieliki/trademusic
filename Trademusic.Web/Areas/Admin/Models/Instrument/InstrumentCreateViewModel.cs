﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

using Trademusic.Web.App_GlobalResources;

namespace Trademusic.Web.Areas.Admin.Models.Instrument
{
    public class InstrumentCreateViewModel
    {
        [Display(ResourceType = typeof(Language), Name = "ADDRESS")]
        public CORE.Domain.Classes.Instrument Instrument { get; set; }

        [Display(ResourceType = typeof(Language), Name = "Family")]
        public List<SelectListItem> Family { get; set; }

        [Display(ResourceType = typeof(Language), Name = "IMAGE")]
        public List<HttpPostedFileBase> Images { get; set; }

        [Display(ResourceType = typeof(Language), Name = "IMAGES")]
        public List<string> ImagesFiles { get; set; }
    }
}