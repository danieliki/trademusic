﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Trademusic.Web.Areas.Admin.Models.Message
{
    public class MessageCreateViewModel
    {
        public CORE.Domain.Classes.Message Message { get; set; }

        public List<SelectListItem> Instrument { get; set; }
    }
}