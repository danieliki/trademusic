﻿using System;

namespace Trademusic.Web.Areas.Admin.Models.Message
{
    public class MessageIndexViewModel
    {
        public int Id { get; set; }

        public string Instrument { get; set; }

        public string UserEmail { get; set; }

        public DateTime Date { get; set; }

        public string Text { get; set; }
    }
}