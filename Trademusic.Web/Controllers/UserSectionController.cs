﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;
using Trademusic.Web.JQueryDataTable;
using Trademusic.Web.Models.UserSection;
using System.Web;
using Trademusic.Web.App_GlobalResources;
using System.Globalization;

namespace Trademusic.Web.Controllers
{
    [Authorize]
    public class UserSectionController : Controller
    {
        private IInstrumentManager instrumentManager;
        private IFamilyManager familyManager;
        private IUserManager userManager;
        private IFavoriteManager favoriteManager;
        private IMessageManager messageManager;
        private string _userId = String.Empty;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="instrumentManager"></param>
        /// <param name="familyManager"></param>
        /// <param name="userManager"></param>
        /// <param name="favoriteManager"></param>
        /// <param name="messageManager"></param>
        public UserSectionController(IInstrumentManager instrumentManager, IFamilyManager familyManager, IUserManager userManager, IFavoriteManager favoriteManager, IMessageManager messageManager)
        {
            this.instrumentManager = instrumentManager;
            this.familyManager = familyManager;
            this.userManager = userManager;
            this.favoriteManager = favoriteManager;
            this.messageManager = messageManager;
        }

        /// <summary>
        /// Método que pinta la pantalla principal de la sección de usuario
        /// </summary>
        /// <returns></returns>
        public ActionResult Main()
        {
            MainViewModel model = new MainViewModel();
            model.UserId = User.Identity.GetUserId();
            model.User = userManager.GetById(model.UserId);

            //ViewBags para la validación por JQuery de los botones: Listado de instrumentos, Favoritos y Mensajes
            ViewBag.messages = messageManager.GetAllByOwnerId(model.UserId).Count();
            ViewBag.instruments = instrumentManager.GetAllByUserId(model.UserId).Count();
            ViewBag.favorites = favoriteManager.GetAllByUserId(model.UserId).Count();

            UserData();

            return View(model);
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult InstrumentList()
        {
            _userId = User.Identity.GetUserId();
            UserData();

            var model =
                instrumentManager.GetAllByUserId(_userId)
                    .Select(e => new FavoritesViewModel {
                        Id = e.Id,
                        Brand = e.Brand,
                        Model = e.Model,
                        Price = e.Price,
                        Date = e.DateTime,
                        User = e.User
                    });

            return View(model);
        }

        /// <summary>
        /// Método que pinta la vista de creación de un instrumento
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateInstrument()
        {
            UserData();

            InstrumentCreateViewModel model = new InstrumentCreateViewModel();
            model = PrepareViewModel(model);

            return View(model);
        }

        /// <summary>
        /// Método que crea un instrumento
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateInstrument(InstrumentCreateViewModel model)
        {
            UserData();
            try
            {
                if (ModelState.IsValid)
                {
                    model.Instrument.UserId = User.Identity.GetUserId();
                    model.Instrument.DateTime = DateTime.Today;
                    instrumentManager.Add(model.Instrument);
                    instrumentManager.db.SaveChanges();

                    if (model.Images != null)
                    {
                        foreach (var image in model.Images)
                        {
                            image.SaveAs(
                                Path.Combine(
                                    @Server.MapPath("~\\Images\\Instruments\\"),
                                    model.Instrument.Id + "_" + model.Images.IndexOf(image) + ".jpg"));
                        }
                    }
                    instrumentManager.Edit(model.Instrument.Id, model.Instrument);
                    instrumentManager.db.SaveChanges();
                    return RedirectToAction("Main");
                }
                model = PrepareViewModel(model);
                return View(model);
            }
            catch (Exception e)
            {
                return View("Error" + e);
            }
        }

        /// <summary>
        /// Método que pinta la vista de edición de un instrumento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EditInstrument(int id)
        {
            InstrumentCreateViewModel model = new InstrumentCreateViewModel();
            model = PrepareViewModel(model);
            model.Instrument = instrumentManager.GetById(id);
            model.ImagesFiles = new List<string>();
            UserData();

            var referenceUri = new Uri(@Server.MapPath("~\\Images\\Instruments\\"));

            foreach (var file in Directory.GetFiles(@Server.MapPath("~\\Images\\Instruments\\"), id + "_*"))
            {
                var fileUri = new Uri(file);
                model.ImagesFiles.Add("/Images/Instruments/" + referenceUri.MakeRelativeUri(fileUri));
            }
            return View(model);
        }

        /// <summary>
        /// Método de edición de un instrumento
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditInstrument(int id, InstrumentCreateViewModel model)
        {
            var instrument = instrumentManager.GetById(id);
            model.Instrument.UserId = instrument.User.Id;
            model.Instrument.DateTime = instrument.DateTime;

            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Images != null)
                    {
                        foreach (var image in model.Images)
                        {
                            if (image != null)
                            {
                                image.SaveAs(Path.Combine(@Server.MapPath("~\\Images\\Instruments\\"), id + "_" + model.Images.IndexOf(image) + ".jpg"));
                            }
                        }
                    }
                   
                    instrumentManager.Edit(id, model.Instrument);
                    instrumentManager.db.SaveChanges();
                    model = PrepareViewModel(model);
                    return RedirectToAction("InstrumentList");
                }
                return View(model);
            }
            catch (Exception e)
            {
                return View("Error" + e);
            }
        }

        /// <summary>
        /// Método que devuelve todos los favoritos en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult Favorites()
        {
            var userId = User.Identity.GetUserId();
            UserData();
            var model =
                favoriteManager.GetAllByUserId(userId)
                    .Select(e => new FavoritesViewModel {
                        Id = e.Instrument.Id,
                        FavoriteId = e.Id,
                        Brand = e.Instrument.Brand,
                        Model = e.Instrument.Model,
                        Price = e.Instrument.Price,
                        User = e.User,
                        City = e.Instrument.User.City              
                    });

            return View(model);
        }

        /// <summary>
        /// Método que añade un favorito al listado de favoritos del usuario
        /// </summary>
        /// <returns></returns>
        public ActionResult AddToFavorites(int id)
        {
            var userId = User.Identity.GetUserId();
            var user = userManager.GetById(userId);
            var instrument = instrumentManager.GetById(id);

            try
            {
                Favorite model = new Favorite { Instrument = instrument, User = user };

                favoriteManager.Add(model);
                favoriteManager.db.SaveChanges();

                return RedirectToAction("Favorites");
            }
            catch (Exception e)
            {
                return View("Error" + e);
            }
        }

        /// <summary>
        /// Método que elimina por llamada Ajax un favorito
        /// </summary>
        /// <param name="favoriteId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RemoveFromFavorites(int favoriteId)
        {
            var favorite = favoriteManager.GetAll().Single(f => f.Id == favoriteId);

            if (favorite != null)
            {
                favoriteManager.Delete(favorite);
                favoriteManager.db.SaveChanges();
            }

            var results = new FavoritesRemoveViewModel
            {
                DeleteId = favoriteId
            };
            return Json(results);
        }

        /// <summary>
        /// Método que contiene los datos del usuario (Datos + Imagen) para la sección izquierda de las pantallas de la sección de usuario
        /// </summary>
        public void UserData()
        {
            _userId = User.Identity.GetUserId();
            ViewBag.User = userManager.GetById(_userId);
     
            var imgFile = string.Empty;

            if (System.IO.File.Exists(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "/Images/Users/" + _userId + ".jpg"))
            { 
                imgFile = "/Images/Users/" + _userId + ".jpg";
            }
            else
            {
                imgFile = "/Images/Users/avatar.png";
            }

            ViewBag.Image = imgFile;
            ViewBag.messages = messageManager.GetAllByOwnerId(_userId).Count();
            ViewBag.instruments = instrumentManager.GetAllByUserId(_userId).Count();
            ViewBag.favorites = favoriteManager.GetAllByUserId(_userId).Count();
        }

        /// <summary>
        /// Método que devuelve todoslos mensajes que le han enviado al usuario
        /// </summary>
        /// <returns></returns>
        public ActionResult MessageList()
        {
            var ownerId = User.Identity.GetUserId();
            UserData();

            var messages = messageManager.GetAllByOwnerId(ownerId).Select(e => new MessageViewModel {
                Id = e.Id,
                User = e.User.Name,
                Date = e.DateTime,
                Text = e.Text,
                Owner = e.OwnerId,
                UserId = e.UserId,
                Instrument = e.Instrument,
                ImageFile = "/Images/Users/" + e.UserId + ".jpg"
            });

            return View(messages);
        }

        /// <summary>
        /// Método que pinta los detalles de uno de los mensajes que le han enviado al usuario
        /// </summary>
        /// <returns></returns>
        public ActionResult MessageDetails(int id)
        {
            var message = messageManager.GetById(id);
            var user = userManager.GetById(message.UserId);
            var ownerId = User.Identity.GetUserId();
            UserData();
            MessageViewModel model = new MessageViewModel()
            {
                Id = id,
                UserId = message.UserId,
                User = user.Name,
                Text = message.Text,
                Date = message.DateTime,
                Owner = ownerId,
                Instrument = message.Instrument,
                ImageFile = "/Images/Users/" + message.UserId + ".jpg"
            };

            return View(model);
        }
        
        /// <summary>
        /// Método que crea un mensaje - respuesta a uno de los mensajes 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MessageDetails(MessageViewModel model, int id)
        {
            var userId = User.Identity.GetUserId();
            var message = messageManager.GetById(id);

            try
            {
                if (ModelState.IsValid)
                {
                    model.Message.OwnerId = message.UserId;
                    model.Message.InstrumentId = message.InstrumentId;
                    model.Message.UserId = userId;
                    model.Message.DateTime = DateTime.Now;
                    model.Message.Read = false;

                    messageManager.Add(model.Message);
                    messageManager.db.SaveChanges();

                    return RedirectToAction("MessageList");
                }
                return View("MessageList");
            }
            catch (Exception e)
            {
                return View("Error" + e);
            }   
        }

        /// <summary>
        /// Método que elimina cambia al estaado leído un mensaje, cuando el usuario lo abre por primera vez
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MessageRead(int id)
        {
            var message = messageManager.GetById(id);

            if (message != null)
            {
                message.Read = true;
                messageManager.Edit(id, message);
                messageManager.db.SaveChanges();
            }

            _userId = User.Identity.GetUserId();
            var messagesCount = messageManager.GetAllByOwnerIdNoRead(_userId).Count();

            var results = new MessageRemoveViewModel
            {   
                MessagesCount = messagesCount,
                DeleteId = id
            };

            return Json(results);
        }

        /// <summary>
        /// Método que pinta la vista de edición de los datos de un usuario
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public ActionResult EditUser()
        {
            UserCreateViewModel model = new UserCreateViewModel();
            var id = User.Identity.GetUserId();
            model.User = userManager.GetById(id);
            UserData();

            return View(model);
        }

        /// <summary>
        /// Método de edición de los datos de un usuario
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditUser(UserCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _userId = User.Identity.GetUserId();
                    var user = userManager.GetById(_userId);

                    user.Name = model.User.Name;
                    user.SurName1 = model.User.SurName1;
                    user.SurName2 = model.User.SurName2;
                    user.Address = model.User.Address;
                    user.ZipCode = model.User.ZipCode;
                    user.City = model.User.City;
                    user.Email = model.User.Email;

                    if (model.Image != null)
                    {
                        model.Image.SaveAs(Path.Combine(@Server.MapPath("~\\Images\\Users\\"), _userId + ".jpg"));
                    }

                    userManager.Edit(_userId, user);
                    userManager.db.SaveChanges();

                    return RedirectToAction("Main");
                }
                
                return View(model);
            }
            catch (Exception e)
            {
                return View("Error" + e);
            }
        }

        /// <summary>
        /// Método que prepara los dropdownlist de la vista
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private InstrumentCreateViewModel PrepareViewModel(InstrumentCreateViewModel model)
        {
            var language = CultureInfo.CurrentUICulture.Name.ToString();

            if (language == "en")
            {
                model.Families =
               familyManager.GetAll().Where(f => f.ParentId == 2 || f.ParentId == 3 || f.ParentId == 4)
                   .Select(f => new SelectListItem { Value = f.Id.ToString(), Text = f.EnglishName })
                   .ToList();
                model.Families.Insert(
                    0,
                    new SelectListItem { Value = "0", Text = "Selecciona una opción", Selected = true });
            }
            else
            {
                model.Families =
               familyManager.GetAll().Where(f => f.ParentId == 2 || f.ParentId == 3 || f.ParentId == 4)
                   .Select(f => new SelectListItem { Value = f.Id.ToString(), Text = f.Name })
                   .ToList();
                model.Families.Insert(
                    0,
                    new SelectListItem { Value = "0", Text = "Selecciona una opción", Selected = true });
            }
           
            return model;
        }

        /// <summary>
        /// Método que muestra en el menú el número de mensajes que hay sin leer
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult MessageSummary()
        {
            _userId = User.Identity.GetUserId();
            var messagesCount = 0;

            if (_userId != null)
            {
                messagesCount = messageManager.GetAllByOwnerIdNoRead(_userId).Count();
            }  
                 
            ViewData["MessagesCount"] = messagesCount;
            return PartialView("_MessageSummary");
        }

        /// <summary>
        /// Método que muestra el menú lateral de la sección de usuario
        /// </summary>
        /// <returns></returns>
        public ActionResult LeftSideMenuUserSection()
        {
            return PartialView("_LeftSideMenuUserSection");
        }
    }
}
