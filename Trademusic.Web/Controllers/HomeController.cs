﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services;
using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;
using Trademusic.Web.Models.App;

namespace Trademusic.Web.Controllers
{
    public class HomeController : Controller
    {

        private IInstrumentManager instrumentManager;
        private IUserManager userManager;
        private IFamilyManager familyManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="instrumentManager"></param>
        /// <param name="userManager"></param>
        /// <param name="familyManager"></param>
        public HomeController(IInstrumentManager instrumentManager, IUserManager userManager, IFamilyManager familyManager)
        {
            this.instrumentManager = instrumentManager;
            this.userManager = userManager;
            this.familyManager = familyManager;
        }

        /// <summary>
        /// Método que devuelve la vista inicial de la aplicación
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Método que devuelve la vista de ayuda
        /// </summary>
        /// <returns></returns>
        public ActionResult Help()
        {
            return View("Help");
        }

        /// <summary>
        /// Método que devuelve la vista del mapa
        /// </summary>
        /// <returns></returns>
        public ActionResult Map()
        {
            return View();
        }

        /// <summary>
        /// Método que cambia el lenguaje de la aplicación
        /// </summary>
        /// <param name="langAbrvtion"></param>
        /// <returns></returns>
        public ActionResult Change(string langAbrvtion)
        {
            if (langAbrvtion != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(langAbrvtion);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(langAbrvtion);
            }

            HttpCookie cookie = new HttpCookie("lang");
            cookie.Value = langAbrvtion;
            Response.Cookies.Add(cookie);

            return View("Index");
        }

        /// <summary>
        /// Método que pinta todas las familias y subfamilias de una variedad en una vista parcial para crear un menú
        /// </summary>
        /// <returns></returns>
        public ActionResult Menu()
        {
            return PartialView("_Menu");
        }

        /// <summary>
        /// Método que devuelve la vista de las normas de compra-venta
        /// </summary>
        /// <returns></returns>
        public ActionResult TradingRules()
        {
            return View("TradingRules");
        }

        /// <summary>
        /// Método de llena las propiedades de la clase FamilyViewModel, deuna manera recursiva
        /// </summary>
        /// <param name="families"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        private List<FamilyMenuViewModel> GetElements(List<Family> families, int? parentId = null)
        {
            List<FamilyMenuViewModel> result = new List<FamilyMenuViewModel>();
            foreach (var family in families.Where(f => f.ParentId == parentId))
            {
                result.Add(new FamilyMenuViewModel {
                    ParentFamily = family,
                    SonFamily = GetElements(families, family.Id)
                });
            }
            return result;
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos en formato JSon para pintarlos en el mapa
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        [WebMethod]
        public string MapData(string city)
        {
            if (city == null)
            {
                throw new ArgumentNullException(nameof(city));
            }
            List<Instrument> instruments = instrumentManager.GetAllByUserCity(city).ToList();
            List<InstrumentMapViewModel> instrumentInMap = new List<InstrumentMapViewModel>();

            foreach (var instrument in instruments)
            {
                var user = userManager.GetById(instrument.UserId);
                instrumentInMap.Add(new InstrumentMapViewModel
                {
                    Id = instrument.Id,
                    Latitude = user.Latitude,
                    Longitude = user.Longitude,
                    Name = user.Name,
                    Address = string.Format("<p><br />{0}<br />Ciudad: {1} </p>", user.Address, user.City),
                    Brand = instrument.Brand,
                    Model = instrument.Model,
                    Price = instrument.Price,
                    URL = "/App/Details/" + instrument.Id.ToString(),
                    Image = "/images/instruments/" + instrument.Id.ToString() + "_0.jpg"
                });
            }
            var jsonSerialiser = new JavaScriptSerializer();
            return jsonSerialiser.Serialize(instrumentInMap);
        }
    }
}