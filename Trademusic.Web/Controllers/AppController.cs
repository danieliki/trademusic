﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using PagedList;
using System.Linq;
using System.Web.Mvc;
using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;
using Trademusic.Web.Models.App;

namespace Trademusic.Web.Controllers
{
    public class AppController : Controller
    {
        private IInstrumentManager instrumentManager;
        private IFamilyManager familyManager;
        private IMessageManager messageManager;
        private IUserManager userManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="instrumentManager"></param>
        /// <param name="familyManager"></param>
        /// <param name="messageManager"></param>
        /// <param name="userManager"></param>
        public AppController(IInstrumentManager instrumentManager, IFamilyManager familyManager, IMessageManager messageManager, IUserManager userManager)
        {
            this.instrumentManager = instrumentManager;
            this.familyManager = familyManager;
            this.messageManager = messageManager;
            this.userManager = userManager;
        }

        /// <summary>
        /// Método para mostrar un listado de instrumentos por familia + paginación
        /// </summary>
        /// <param name="family"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult Index(int family, int? page, string currentFilter, string searchString)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else { searchString = currentFilter; }
            ViewBag.CurrentFilter = searchString;

            var fam = familyManager.GetById(family);

            var model = instrumentManager.GetAllByFamily(family)
                    .Select(p => new InstrumentListViewModel {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Brand = p.Brand,
                        Price = p.Price,
                        Description = p.Description,
                        Family = fam.Name,
                        EngFamily = fam.EnglishName,
                        DateTime = p.DateTime,
                        User = p.User
                    }).ToList();

            ViewBag.family = family;
            int pageSize = 4;
            int pageNumber = (page ?? 1);

            return View(model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Método para mostrar un listado de instrumentos por familia y marca + paginación
        /// </summary>
        /// <param name="family"></param>
        /// <param name="brand"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult IndexBrand(int family, string brand, int? page)
        {
            var fam = familyManager.GetById(family);

            var model = instrumentManager.GetAllByFamilyAndBrand(family, brand)
                    .Select(p => new InstrumentListViewModel
                    {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Brand = p.Brand,
                        Price = p.Price,
                        Description = p.Description,
                        Family = fam.Name,
                        EngFamily = fam.EnglishName,
                        DateTime = p.DateTime,
                        User = p.User
                    }).ToList();
            ViewBag.family = family;
            int pageSize = 4;
            int pageNumber = (page ?? 1);

            return View("Index", model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Método para mostrar un listado de instrumentos por familia y ciudad + paginación
        /// </summary>
        /// <param name="family"></param>
        /// <param name="city"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult IndexCity(int family, string city, int? page)
        {
            var fam = familyManager.GetById(family);

            var model = instrumentManager.GetAllByFamilyAndCity(family, city)
                    .Select(p => new InstrumentListViewModel
                    {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Brand = p.Brand,
                        Price = p.Price,
                        Description = p.Description,
                        Family = fam.Name,
                        EngFamily = fam.EnglishName,
                        DateTime = p.DateTime,
                        User = p.User
                    }).ToList();

            ViewBag.family = family;
            ViewBag.city = city;
            int pageSize = 4;
            int pageNumber = (page ?? 1);

            return View("Index", model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Método para mostrar un listado de instrumentos por familia y rango de precios + paginación
        /// </summary>
        /// <param name="family"></param>
        /// <param name="price1"></param>
        /// <param name="price2"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult IndexPriceRange(int family, int price1, int price2, int? page)
        {
            var fam = familyManager.GetById(family);

            var model = instrumentManager.GetAllByFamilyAndPriceRange(family, price1, price2)
                    .Select(p => new InstrumentListViewModel
                    {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Brand = p.Brand,
                        Price = p.Price,
                        Description = p.Description,
                        Family = fam.Name,
                        EngFamily = fam.EnglishName,
                        DateTime = p.DateTime,
                        User = p.User
                    }).ToList();
            ViewBag.family = family;
            ViewBag.price1 = price1;
            ViewBag.price2 = price2;
            int pageSize = 4;
            int pageNumber = (page ?? 1);

            return View("Index", model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Método de autocompetado del cuadro de búsqueda
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        //public JsonResult AutoCompleteInstrument(string term)
        //{
        //    var result = instrumentManager.GetAll().Where(i => i.Brand.ToLower().Contains(term.ToLower())).Select(i => i.Brand).Distinct();

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        public List<string> Search(string name)
        {
            var result = instrumentManager.GetAll().Where(i => i.Brand.StartsWith(name)).Select(i => i.Brand).ToList();

            return result;
        }

        /// <summary>
        /// Método para filtrar los instrumentos por familia
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ListByFamily(int id)
        {
            var model = instrumentManager.GetAllByFamily(id)
                    .Select(p => new InstrumentListViewModel {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Brand = p.Brand,
                        Price = p.Price,
                        Description = p.Description,
                        Family = p.Family.Name,
                        EngFamily = p.Family.EnglishName,
                        DateTime = p.DateTime,
                        User = p.User
                    }).ToList();

            return PartialView("_ListPartial", model);
        }

        /// <summary>
        /// Método de llena las propiedades de la clase FamilyViewModel, de una manera recursiva
        /// </summary>
        /// <param name="families"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        private List<FamilyMenuViewModel> GetElements(List<Family> families, int? parentId = null)
        {
            List<FamilyMenuViewModel> result = new List<FamilyMenuViewModel>();
            foreach (var family in families.Where(f => f.ParentId == parentId))
            {
                result.Add(new FamilyMenuViewModel {
                    ParentFamily = family,
                    SonFamily = GetElements(families, family.Id)
                });
            }
            return result;
        }

        /// <summary>
        /// Método que pinta todas las familias y subfamilias de un instrumento en una vista parcial para fitrar por ellas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FamilyMenu()
        {
            var families = familyManager.GetAll().ToList();
            FamilyMenuViewModel model = GetElements(families).FirstOrDefault();

            ViewData["userLanguage"] = CultureInfo.CurrentUICulture.Name.ToString();

            return PartialView("_FamilyMenu", model);
        }

        /// <summary>
        /// Método para filtrar los instrumentos por marca
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ListByBrand(string brand)
        {
            var model = instrumentManager.GetAllByBrand(brand)
                    .Select(p => new InstrumentListViewModel {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Brand = p.Brand,
                        Price = p.Price,
                        Description = p.Description,
                        Family = p.Family.Name,
                        EngFamily = p.Family.EnglishName,
                        DateTime = p.DateTime,
                        User = p.User
                    }).ToList();

            return PartialView("_ListPartial", model);
        }

        /// <summary>
        /// Método para filtrar los instrumentos por rango de precios
        /// </summary>
        /// <param name="price1"></param>
        /// <param name="price2"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ListByPrice(decimal price1, decimal price2)
        {
            var model = instrumentManager.GetAllByPriceRange(price1, price2)
                    .Select(p => new InstrumentListViewModel {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Brand = p.Brand,
                        Price = p.Price,
                        Description = p.Description,
                        Family = p.Family.Name,
                        EngFamily = p.Family.EnglishName,
                        DateTime = p.DateTime,
                        User = p.User
                    }).ToList();

            return PartialView("_ListPartial", model);
        }

        /// <summary>
        /// Método para filtrar los instrumentos por ciudad
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ListByCity(string city)
        {
            var model = instrumentManager.GetAllByUserCity(city)
                    .Select(p => new InstrumentListViewModel
                    {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Brand = p.Brand,
                        Price = p.Price,
                        Description = p.Description,
                        Family = p.Family.Name,
                        EngFamily = p.Family.EnglishName,
                        DateTime = p.DateTime,
                        User = p.User
                    }).ToList();

            return PartialView("_ListPartial", model);
        }

        /// <summary>
        /// Método que muestra los detalles de un instrumento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            var instrument = instrumentManager.GetById(id);
            var messages = messageManager.GetAllByInstrumentId(id).ToList();
            var referenceUri = new Uri(@Server.MapPath("~\\Images\\Instruments\\"));
            var imageFiles = Directory.GetFiles(@Server.MapPath("~\\Images\\Instruments\\"), id + "_*")
                .Select(file => new Uri(file)).Select(fileUri => "/Images/Instruments/" + referenceUri.MakeRelativeUri(fileUri).ToString()).ToList();
            var userId = instrument.UserId;
            var imgFile = string.Empty;

            if (System.IO.File.Exists(System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "/Images/Users/" + userId + ".jpg"))
            {
                imgFile = "/Images/Users/" + userId + ".jpg";
            }
            else
            {
                imgFile = "/Images/Users/avatar.png";
            }

            ViewBag.UserImage = imgFile;

            InstrumentDetailsViewModel model = new InstrumentDetailsViewModel {
                Id = id,
                Instrument = instrument,
                UserId = userId,
                ImagesFiles = imageFiles,
                Image = imageFiles.FirstOrDefault(),
                User = userManager.GetById(instrument.UserId)
            };

            return View(model);
        }

        /// <summary>
        /// Método que pinta la pantalla modal de creción de un mensaje
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult MessageModal(int id)
        {
            InstrumentDetailsViewModel model = new InstrumentDetailsViewModel();
            model.Id = id;

            return PartialView("_MessageModal", model);
        }

        /// <summary>
        /// Método de creación de un mensaje
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MessageModalResponse(InstrumentDetailsViewModel model, int id)
        {
            ApplicationUser user;

            if (!User.Identity.IsAuthenticated)
            {
                try
                {
                    user = new ApplicationUser()
                    {
                        Name = model.UserName,
                        Email = model.UserEmail,
                        UserName = model.UserEmail,
                        EmailConfirmed = false,
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnabled = true,
                        AccessFailedCount = 0
                    };

                    userManager.Add(user);
                    userManager.db.SaveChanges();
                }
                catch (Exception e)
                {
                    return View("Error" + e);
                }
            }
            else
            {
                var userId = User.Identity.GetUserId();
                user = userManager.GetById(userId);
            }

            var instrument = instrumentManager.GetById(model.Id);
            var owner = userManager.GetById(instrument.UserId);

            try
            {
                if (ModelState.IsValid)
                {
                    var message = new Message
                    {
                        Id = new int(),
                        User = user,
                        DateTime = DateTime.Now,
                        InstrumentId = model.Id,
                        Text = model.Text,
                        OwnerId = owner.Id
                    };

                    messageManager.Add(message);
                    messageManager.db.SaveChanges();

                    return RedirectToAction("Details", new { id = model.Id });
                }
                return PartialView("_MessageModal", model);
            }
            catch (Exception e)
            {
                return View("Error" + e);
            }
        }

        /// <summary>
        /// Método que pinta pos datos del usuario que vende el instrumento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult UserProfile(int id)
        {
            var instrument = instrumentManager.GetById(id);
            InstrumentDetailsViewModel model = new InstrumentDetailsViewModel()
            {
                Id = id,
                Instrument = instrument,
                UserId = instrument.UserId,
                User = userManager.GetById(instrument.UserId)
            };
            return PartialView("_UserProfile", model);
        }
    }
}
