﻿using System.Web.Optimization;

namespace Trademusic.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Scripts

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-2.2.3.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.validate*",
                    "~/Scripts/jQueryFixes.js",
                    "~/Scripts/jquery.validate.unobtrusive.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js", "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include("~/Scripts/DataTables/jquery.dataTables.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/template").Include(
                    "~/Scripts/main.js",
                    "~/Scripts/jquery.scrollUp.min.js",
                    "~/Scripts/jquery.prettyPhoto.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/zoom").Include(
                    "~/Scripts/jquery.elevatezoom.js"
                    //,
                    //"~/Scripts/jquery.elevateZoom-3.0.8.min.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/moment").Include("~/Scripts/moment.min.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/filer").Include(
                    "~/Scripts/JQueryFiler/custom.js",
                    "~/Scripts/JQueryFiler/jquery.filer.js",
                    "~/Scripts/JQueryFiler/jquery.filer.min.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/creative").Include(
                    "~/Scripts/Creative/scrollreveal.js",
                    "~/Scripts/Creative/scrollreveal.min.js",
                    "~/Scripts/Creative/jquery.easing.min.js",
                    "~/Scripts/Creative/jquery.fittext.js",
                    "~/Scripts/Creative/jquery.magnific-popup.js",
                    "~/Scripts/Creative/jquery.magnific-popup.min.js", 
                    "~/Scripts/Creative/creative.js"));

            #endregion

            #region Modernizr

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            #endregion

            #region Content css

            bundles.Add(new StyleBundle("~/Content/css").Include(
                    "~/Content/site.css"));

            bundles.Add(
                new StyleBundle("~/Content/filer").Include(
                    "~/Content/JQueryFiler/jquery-filer.css",
                    "~/Content/JQueryFiler/jquery-filer.eot",
                    "~/Content/JQueryFiler/jquery-filer.svg",
                    "~/Content/JQueryFiler/jquery-filer.ttf",
                    "~/Content/JQueryFiler/jquery-filer.woff",
                    "~/Content/JQueryFiler/jquery.filer-dragdropbox-theme.css",
                    "~/Content/JQueryFiler/jquery.filer.css"));

            bundles.Add(
                new StyleBundle("~/Content/creative").Include(
                    "~/Content/creative.css",
                    "~/Content/font-awesome/css/font-awesome.min.css",
                    "~/Content/magnific-popup.css"));

            bundles.Add(
                new StyleBundle("~/Content/template").Include(
                    "~/Content/main.css",
                    "~/Content/prettyPhoto.css",
                    "~/Content/responsive.css",
                    "~/Content/animate.css"));
            #endregion
        }
    }
}
