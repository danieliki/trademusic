﻿using System.Collections.Generic;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Web.Models.App
{
    public class InstrumentDetailsViewModel
    {
        public int Id { get; set; }

        public Instrument Instrument { get; set; }

        public string Image { get; set; }

        public List<string> ImagesFiles { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public string UserEmail { get; set; }

        public ApplicationUser User { get; set; }

        public string Text { get; set; }

        public string UserImage
        {
            get
            {
                return "/Images/Users/" + UserId + ".jpg";
            }
            private set { }
        }
    }
}