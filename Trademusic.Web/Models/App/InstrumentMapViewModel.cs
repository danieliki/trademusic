﻿
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Web.Models.App
{
    public class InstrumentMapViewModel
    {
        public int Id { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Address { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public decimal Price { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        public string URL { get; set; }

        public ApplicationUser User { get; set; }
    }
}