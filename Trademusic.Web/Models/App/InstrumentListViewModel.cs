﻿using System;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Web.Models.App
{
    public class InstrumentListViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Brand { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public DateTime DateTime { get; set; }

        public ApplicationUser User { get; set; }

        public string Family { get; set; }

        public string EngFamily { get; set; }

        public string Image
        {
            get
            {
                return "/Images/Instruments/" + BuildNameImage(Id);
            }
            private set { }
        }

        private string BuildNameImage(int varietyId)
        {
            return $"{varietyId}_0.jpg";
        }
    }
}