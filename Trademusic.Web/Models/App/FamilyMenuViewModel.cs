﻿using System.Collections.Generic;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Web.Models.App
{
    public class FamilyMenuViewModel
    {
        public Family ParentFamily { get; set; }
        public List<FamilyMenuViewModel> SonFamily { get; set; }
    }
}