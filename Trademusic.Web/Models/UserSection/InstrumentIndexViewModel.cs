﻿using System.ComponentModel.DataAnnotations;

namespace Trademusic.Web.Models.UserSection
{
    public class InstrumentIndexViewModel
    {
        public int Id { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        [DisplayFormat(DataFormatString = "{0:0,00}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        public string User { get; set; }

        public string ImageFile { get; set; }

        public string Image
        {
            get
            {
                return "/Images/Instruments/" + BuildNameImage(Id);
            }
            private set { }
        }

        private string BuildNameImage(int varietyId)
        {
            return $"{varietyId}_0.jpg";
        }
    }
}