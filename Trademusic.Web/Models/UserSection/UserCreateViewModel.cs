﻿using System.Web;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Web.Models.UserSection
{
    public class UserCreateViewModel
    {
        public ApplicationUser User { get; set; }

        public HttpPostedFileBase Image { get; set; }

        public string ImageFile { get; set; }
    }
}