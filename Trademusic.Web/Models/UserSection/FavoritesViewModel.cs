﻿using System;
using System.ComponentModel.DataAnnotations;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Web.Models.UserSection
{
    public class FavoritesViewModel
    {
        public int Id { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public DateTime Date { get; set; }

        [DisplayFormat(DataFormatString = "{0:0,00}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        public ApplicationUser User { get; set; }

        public string City { get; set; }

        public bool IsSold { get; set; }

        public int FavoriteId { get; set; }

        public Favorite Favorite { get; set; }

        public string ImageFile { get; set; }

        public string Image
        {
            get
            {
                return "/Images/Instruments/" + BuildNameImage(Id);
            }
            private set { }
        }

        private string BuildNameImage(int varietyId)
        {
            return $"{varietyId}_0.jpg";
        }
    }
}