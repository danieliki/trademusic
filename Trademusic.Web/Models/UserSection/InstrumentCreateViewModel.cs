﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Web.Models.UserSection
{
    public class InstrumentCreateViewModel
    {
        public Instrument Instrument { get; set; }

        public List<SelectListItem> Families { get; set; }

        public List<HttpPostedFileBase> Images { get; set; }

        public List<string> ImagesFiles { get; set; }

        public string ImageFile { get; set; }

        public ApplicationUser User { get; set; }
    }
}