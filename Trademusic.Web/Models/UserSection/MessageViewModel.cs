﻿using System;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Web.Models.UserSection
{
    public class MessageViewModel
    {
        public int Id { get; set; }

        public string User { get; set; }

        public string UserId { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }

        public string Owner { get; set; }

        public Message Message { get; set; }

        public Instrument Instrument { get; set; }

        public string UserImage
        {
            get
            {
                return "/Images/Users/" + BuildNameImageUser(UserId);
            }
            private set { }
        }

        private string BuildNameImageUser(string userId)
        {
            return $"{userId}.jpg";
        }

        public string OwnerImage
        {
            get
            {
                return "/Images/Users/" + BuildNameImageOwner(Owner);
            }
            private set { }
        }

        private string BuildNameImageOwner(string ownerId)
        {
            return $"{ownerId}.jpg";
        }

        public string ImageFile { get; set; }
    }
}