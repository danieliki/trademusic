﻿namespace Trademusic.Web.Models.UserSection
{
    public class FavoritesRemoveViewModel
    {
        public int DeleteId { get; set; }
    }
}