﻿using System.Web;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.Web.Models.UserSection
{
    public class MainViewModel
    {
        public ApplicationUser User { get; set; }

        public string UserId { get; set; }

        public HttpPostedFileBase Image { get; set; }

        public string ImageFile { get; set; }

        public bool Favoritos { get; set; }

        public bool Instrumentos { get; set; }

        public bool Mensajes { get; set; }
    }
}