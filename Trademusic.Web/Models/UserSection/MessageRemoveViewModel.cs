﻿namespace Trademusic.Web.Models.UserSection
{
    public class MessageRemoveViewModel
    {
        public int MessagesCount { get; set; }
        public int DeleteId { get; set; }
    }
}