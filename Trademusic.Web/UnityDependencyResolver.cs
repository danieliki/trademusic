﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Trademusic.IFR;

namespace Trademusic.Web
{
    public class UnityDependencyResolver : IDependencyResolver
    {
        public object GetService(Type serviceType)
        {
            try
            {
                object instance = IoC.Current.Resolve(serviceType);
                if (serviceType.IsAbstract || serviceType.IsInterface)
                {
                    return null;
                }
                return instance;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return IoC.Current.ResolveAll(serviceType);
        }
    }
}