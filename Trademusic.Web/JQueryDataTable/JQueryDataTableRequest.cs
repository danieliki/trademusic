﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;

namespace Trademusic.Web.JQueryDataTable
{
    /// <summary>
    /// Class that encapsulates most common parameters sent by DataTables plugin
    /// </summary>
    public class JQueryDataTableRequest
    {
        /// <summary>
        /// Request sequence number sent by DataTable,
        /// same value must be returned in response
        /// </summary>       
        public int sEcho { get; set; }

        /// <summary>
        /// Number of columns in table
        /// </summary>
        public int iColumns { get; set; }

        /// <summary>
        /// Comma separated list of column names
        /// </summary>
        public string sColumns { get; set; }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>
        public int iDisplayStart { get; set; }

        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>
        public int iDisplayLength { get; set; }

        public List<string> mDataProp { get; set; }
        public List<string> Search { get; set; }
        public List<string> bRegex { get; set; }
        public List<string> bSearchable { get; set; }
        public List<string> bSortable { get; set; }
        public string sSearch { get; set; }

        /// <summary>
        /// Text used for filtering
        /// </summary>

        public List<string> iSortCol { get; set; }
        public List<string> sSortDir { get; set; }

        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>
        public int iSortingCols { get; set; }

        public JQueryDataTableRequest(HttpRequest request)
        {
            foreach (var param in request.Params)
            {
                if (param != null)
                {
                    string name = param.ToString();
                    int pos = name.IndexOf('_');
                    var property = GetType().GetProperty(name.Substring(0, pos > 0 ? pos : name.Length));
                    if (property != null)
                    {
                        if (property.PropertyType == typeof(List<string>))
                        {
                            if (property.GetValue(this) == null)
                            {
                                property.SetValue(this, new List<string>());
                            }
                            ((IList)property.GetValue(this)).Add(request.Params[name]);
                        }
                        else if (property.PropertyType == typeof(int))
                        {
                            property.SetValue(this, int.Parse(request.Params[name]));
                        }
                        else
                        {
                            property.SetValue(this, request.Params[name]);
                        }
                    }
                    else
                    {
                        Debug.WriteLine(name);
                    }
                }
            } 
        }
    }
}
