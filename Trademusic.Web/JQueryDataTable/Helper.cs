﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;

namespace Trademusic.Web.JQueryDataTable
{
    public static class JQueryHelper
    {
        public static void PrepareParametersSP<T>(JQueryDataTableRequest request, out int currentPage, out int pageSize, out string orderBy, out string orderDirection, out string where)
        {
            currentPage = 1;
            pageSize = 12;
            orderBy = "";
            orderDirection = "asc";
            where = "";

            //Order
            Type type = typeof(T);
            var columns = type.GetProperties().Select(p => p.Name);
            foreach (var sortCol in request.iSortCol)
            {
                var propertyInfo = type.GetProperty(columns.ElementAt(int.Parse(sortCol)));
                orderBy = propertyInfo.Name;
                if (request.sSortDir[request.iSortCol.IndexOf(sortCol)] == "asc")
                    orderDirection = "asc";
                else
                    orderDirection = "desc";
            }

            //Search
            if (!string.IsNullOrWhiteSpace(request.sSearch))
            {
                where = request.sSearch;
            }

            //Paging
            if (request.iDisplayLength >= 0)
            {
                pageSize = request.iDisplayLength;

                currentPage = request.iDisplayStart == 0
                      ? 0
                      : request.iDisplayStart / request.iDisplayLength;
                //list = list.Skip(itemsToSkip).Take(request.iDisplayLength);
            }

        }
        public static JQueryDataTableResponse CreateJQueryDataTableResponseSP<T>(IEnumerable<T> list, JQueryDataTableRequest request)
        {
            JQueryDataTableResponse jsDT = new JQueryDataTableResponse(request);
            jsDT.iTotalDisplayRecords = list.Count();

            //Order
            if (list.Any() && request.iSortCol != null)
            {
                string order = string.Empty;
                Type type = list.First().GetType();
                var columns = type.GetProperties().Select(p => p.Name);
                foreach (var sortCol in request.iSortCol)
                {
                    var propertyInfo = type.GetProperty(columns.ElementAt(int.Parse(sortCol)));
                    if (request.sSortDir[request.iSortCol.IndexOf(sortCol)] == "asc")
                        list = list.OrderBy(x => propertyInfo.GetValue(x, null)).ToList();
                    else
                        list = list.OrderByDescending(x => propertyInfo.GetValue(x, null)).ToList();
                }
            }


            //Search
            if (!string.IsNullOrWhiteSpace(request.sSearch))
            {
                var element = list.FirstOrDefault();
                if (element != null)
                {
                    PropertyInfo[] properties = element.GetType().GetProperties();
                    foreach (var property in properties.Where(p => !p.Name.EndsWith("String")))
                    {
                        //list = list.Filter<T>(property.Name, request.sSearch).ToList();                        
                    }
                }
            }

            //Paging
            if (request.iDisplayLength >= 0)
            {
                var itemsToSkip = request.iDisplayStart == 0
                      ? 0
                      : request.iDisplayStart;
                //list = list.Skip(itemsToSkip).Take(request.iDisplayLength);
            }


            if (list.Any())
            {
                var fistElement = list.FirstOrDefault();
                if (fistElement != null)
                {
                    PropertyInfo[] prps = fistElement.GetType().GetProperties();
                    foreach (var property in prps)
                    {
                        jsDT.add_Column(new JQueryDataTableResponse.JsDataColumn { Title = property.Name, Class = property.PropertyType.Name });
                    }
                    foreach (var element in list)
                    {
                        List<object> vl = new List<object>();
                        foreach (var property in prps)
                        {
                            vl.Add(property.GetValue(element, null));
                        }
                        jsDT.add_Row(vl);
                    }
                }
            }
            jsDT.iTotalRecords = jsDT.iTotalRecords > list.Count() ? list.Count() : jsDT.iTotalRecords;
            return jsDT;
        }
        public static JQueryDataTableResponse CreateJQueryDataTableResponseSP<T>(IEnumerable<T> list, JQueryDataTableRequest request, int total)
        {
            JQueryDataTableResponse jsDT = new JQueryDataTableResponse(request);
            jsDT.iTotalDisplayRecords = total;

            //Order
            //if (list.Any() && request.iSortCol != null)
            //{
            //    string order = string.Empty;
            //    Type type = list.First().GetType();
            //    var columns = type.GetProperties().Select(p => p.Name);
            //    foreach (var sortCol in request.iSortCol)
            //    {
            //        var propertyInfo = type.GetProperty(columns.ElementAt(int.Parse(sortCol)));
            //        if (request.sSortDir[request.iSortCol.IndexOf(sortCol)] == "asc")
            //            list = list.OrderBy(x => propertyInfo.GetValue(x, null)).ToList();
            //        else
            //            list = list.OrderByDescending(x => propertyInfo.GetValue(x, null)).ToList();
            //    }
            //}


            //Search
            //if (!string.IsNullOrWhiteSpace(request.sSearch))
            //{
            //    var element = list.FirstOrDefault();
            //    if (element != null)
            //    {
            //        PropertyInfo[] properties = element.GetType().GetProperties();
            //        foreach (var property in properties.Where(p => !p.Name.EndsWith("String")))
            //        {
            //            //list = list.Filter<T>(property.Name, request.sSearch).ToList();                        
            //        }
            //    }
            //}

            //Paging
            //if (request.iDisplayLength >= 0)
            //{
            //    var itemsToSkip = request.iDisplayStart == 0
            //          ? 0
            //          : request.iDisplayStart;
            //    //list = list.Skip(itemsToSkip).Take(request.iDisplayLength);
            //}


            if (list.Any())
            {
                var fistElement = list.FirstOrDefault();
                if (fistElement != null)
                {
                    PropertyInfo[] prps = fistElement.GetType().GetProperties();
                    foreach (var property in prps)
                    {
                        jsDT.add_Column(new JQueryDataTableResponse.JsDataColumn { Title = property.Name, Class = property.PropertyType.Name });
                    }
                    foreach (var element in list)
                    {
                        List<object> vl = new List<object>();
                        foreach (var property in prps)
                        {
                            vl.Add(property.GetValue(element, null));
                        }
                        jsDT.add_Row(vl);
                    }
                }
            }
            jsDT.iTotalRecords = total;
            return jsDT;
        }
        public static JQueryDataTableResponse CreateJQueryDataTableResponse(IEnumerable<object> list, JQueryDataTableRequest request)
        {
            JQueryDataTableResponse jsDT = new JQueryDataTableResponse(request);
            jsDT.iTotalDisplayRecords = list.Count();

            //Order
            if (list.Any() && request.iSortCol != null)
            {
                string order = string.Empty;
                var columns = list.First().GetType().GetProperties().Select(p => p.Name);
                foreach (var sortCol in request.iSortCol)
                {
                    order += columns.ElementAt(int.Parse(sortCol)) + " " + request.sSortDir[request.iSortCol.IndexOf(sortCol)] + ", ";
                }
                list = list.OrderBy(order.Substring(0, order.Length - 2));
            }


            //Search
            if (!string.IsNullOrWhiteSpace(request.sSearch))
            {
                var element = list.FirstOrDefault();
                if (element != null)
                {
                    PropertyInfo[] properties = element.GetType().GetProperties();

                    //Id.ToString().Contains(@0) or Name.ToString().Contains(@0) or Phone.ToString().Contains(@0) or Email.ToString().Contains(@0) or LastContact.ToString().Contains(@0) or Amount
                    //string contains = string.Join(".ToString().Contains(@0) or ", properties.Select(p => p.Name));
                    string contains = "";
                    foreach (var property in properties.Where(p => !p.Name.EndsWith("String")))
                    {
                        Type type = property.PropertyType;
                        if (type == typeof(Decimal))
                        {
                            contains = contains + property.Name + ".ToString().Contains(@0) or ";
                            if (properties.Where(p => p.Name == property.Name + "String").Any())
                            {
                                contains = contains + property.Name + "String.Contains(@0) or ";
                            }
                        }
                        else if (type == typeof(DateTime))
                        {
                            if (properties.Where(p => p.Name == property.Name + "String").Any())
                            {
                                contains = contains + property.Name + "String.Contains(@0) or ";
                            }
                            else
                            {
                                contains = contains + property.Name + ".ToString().Contains(@0) or ";
                            }
                        }
                        else
                        {
                            contains = contains + property.Name + ".ToString().Contains(@0) or ";
                        }
                    }
                    contains = contains.Substring(0, contains.Length - 3);
                    list = list.Where(contains, request.sSearch);
                    //list = list.Where(contains + ".ToString().Contains(@0)", request.sSearch);
                }
            }

            //Paging
            if (request.iDisplayLength >= 0)
            {
                var itemsToSkip = request.iDisplayStart == 0
                      ? 0
                      : request.iDisplayStart;
                list = list.Skip(itemsToSkip).Take(request.iDisplayLength);
            }


            if (list.Any())
            {
                var fistElement = list.FirstOrDefault();
                if (fistElement != null)
                {
                    PropertyInfo[] prps = fistElement.GetType().GetProperties();
                    foreach (var property in prps)
                    {
                        jsDT.add_Column(new JQueryDataTableResponse.JsDataColumn { Title = property.Name, Class = property.PropertyType.Name });
                    }
                    foreach (var element in list)
                    {
                        List<object> vl = new List<object>();
                        foreach (var property in prps)
                        {
                            vl.Add(property.GetValue(element, null));
                        }
                        jsDT.add_Row(vl);
                    }
                }
            }
            jsDT.iTotalRecords = jsDT.iTotalRecords > list.Count() ? list.Count() : jsDT.iTotalRecords;
            return jsDT;
        }
    }
}