﻿using System.Collections.Generic;

namespace Trademusic.Web.JQueryDataTable
{
    public class JQueryDataTableResponse
    {
        public int sEcho { get; set; }
        public int iTotalRecords { get; set; }
        public int iTotalDisplayRecords { get; set; }

        public List<List<object>> aaData;
        public List<JsDataColumn> aoColumns;

        public JQueryDataTableResponse(JQueryDataTableRequest request)
        {
            sEcho = request.sEcho;
            iTotalRecords = 0;
            iTotalRecords = request.iDisplayLength;
            aaData = new List<List<object>>();
            aoColumns = new List<JsDataColumn>();
        }

        public void add_Row(List<object> cells)
        {
            aaData.Add(cells);
        }

        public class JsDataColumn
        {
            public string Title { get; set; }
            public string Class { get; set; }
        }

        public void add_Column(JsDataColumn col)
        {
            aoColumns.Add(col);
        }
    }
}