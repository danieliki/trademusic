﻿using System;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.WebServices.Models
{
    public class MessageViewModel
    {
        public int Id { get; set; }

        public string User { get; set; }

        public string UserId { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }

        public string OwnerId { get; set; }

        public Message Message { get; set; }

        public Instrument Instrument { get; set; }

        public string Image { get; set; }
    }
}