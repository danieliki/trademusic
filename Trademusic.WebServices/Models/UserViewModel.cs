﻿using Trademusic.CORE.Domain.Classes;

namespace Trademusic.WebServices.Models
{
    public class UserViewModel
    {
        public ApplicationUser User { get; set; }

        public string Image { get; set; }
    }
}