﻿using System.ComponentModel.DataAnnotations;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.WebServices.Models
{
    public class FavoriteViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        [DisplayFormat(DataFormatString = "{0:0,00}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        public ApplicationUser User { get; set; }

        public string City { get; set; }

        public string Description { get; set; }

        public int FavoriteId { get; set; }

        public Favorite Favorite { get; set; }

        public string Image { get; set; }
    }
}