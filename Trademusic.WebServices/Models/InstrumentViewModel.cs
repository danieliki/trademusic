﻿using System;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.WebServices.Models
{
    public class InstrumentViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Brand { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public DateTime DateTime { get; set; }

        public ApplicationUser User { get; set; }

        public string Family { get; set; }

        public string EngFamily { get; set; }

        public string Image { get; set; }

        public string Image1 { get; set; }

        public string Image2 { get; set; }

        public string Image3 { get; set; }

        public string Image4 { get; set; }        
    }
}