﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Trademusic.Application;
using Trademusic.CORE.Contracts;
using Trademusic.DAL;
using Trademusic.WebServices.Models;

namespace Trademusic.WebServices.Controllers
{
    public class FavoriteController : ApiController
    {
        /// <summary>
        /// Método que devuelve todos los favoritos de un usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<FavoriteViewModel> GetFavoritesByUserId(string id)
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IFavoriteManager favoriteManager = new FavoriteManager(db);

            var model = favoriteManager.GetAllByUserId(id).Select(p => new FavoriteViewModel
            {
                Id = p.Id,
                Brand = p.Instrument.Brand,
                Model = p.Instrument.Model,
                Name = p.Instrument.Brand + " - " + p.Instrument.Model,
                Description = p.Instrument.Description,
                Price = p.Instrument.Price,
                City = p.User.City,
                User = p.User,
                Image = "http://trademusic2.azurewebsites.net/images/instruments/" + p.Instrument.Id.ToString() + "_0.jpg"
            }).ToList();

            return model;
        }
    }
}
