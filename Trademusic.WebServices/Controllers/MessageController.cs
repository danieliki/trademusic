﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Trademusic.Application;
using Trademusic.CORE.Contracts;
using Trademusic.DAL;
using Trademusic.WebServices.Models;

namespace Trademusic.WebServices.Controllers
{
    public class MessageController : ApiController
    {
        /// <summary>
        /// Método que devuelve todos los mensajes de un usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<MessageViewModel> GetMessages(string id)
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IMessageManager messageManager = new MessageManager(db);

            var model = messageManager.GetAllByOwnerId(id).Select(p => new MessageViewModel
            {
                Id = p.Id,
                User = p.User.Name,
                UserId = p.UserId,
                Text = p.Text,
                Date = p.DateTime,
                OwnerId = p.OwnerId,
                Instrument = p.Instrument,
                Image = "http://trademusic2.azurewebsites.net/images/users/" + p.UserId + ".jpg"
            }).ToList();

            return model;
        }  
    }
}
