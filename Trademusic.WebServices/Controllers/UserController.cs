﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Trademusic.Application;
using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;
using Trademusic.DAL;
using Trademusic.WebServices.Models;

namespace Trademusic.WebServices.Controllers
{
    public class UserController : ApiController
    {
        /// <summary>
        /// Método que devuelve todos los usuarios
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ApplicationUser> GetUsers()
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IUserManager userManager = new UserManager(db);
            return userManager.GetAll().ToList();
        }

        /// <summary>
        /// Métodoque devuelve un usuario por sus email y contraseña (login)
        /// </summary>
        /// <param name="email"></param>
        /// <param name="pass"></param>
        /// <returns></returns>
        public UserViewModel GetLoginUser(string email, string pass)
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IUserManager userManager = new UserManager(db);
            var user = userManager.GetLoginUser(email, pass);
            UserViewModel model = new UserViewModel()
            {
                User = user,
                Image = "http://trademusic2.azurewebsites.net/images/users/" + user.Id + ".jpg"
            };
            return model;
        }
    }
}
