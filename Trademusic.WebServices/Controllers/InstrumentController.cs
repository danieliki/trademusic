﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Trademusic.Application;
using Trademusic.CORE.Contracts;
using Trademusic.DAL;
using Trademusic.WebServices.Models;

namespace Trademusic.WebServices.Controllers
{
    public class InstrumentController : ApiController
    {
        /// <summary>
        /// Método que devuelve todos los instrumentos
        /// </summary>
        /// <returns></returns>
        public List<InstrumentViewModel> GetInstruments()
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IInstrumentManager instrumentManager = new InstrumentManager(db);

            var model = instrumentManager.GetAll().Select(p => new InstrumentViewModel
            {
                Id = p.Id,
                Name = p.Brand + " - " + p.Model,
                Brand = p.Brand,
                Price = p.Price,
                Description = p.Description,
                Family = p.Family.Name,
                EngFamily = p.Family.EnglishName,
                DateTime = p.DateTime,
                User = p.User,
                Image = "http://trademusic2.azurewebsites.net/images/instruments/" + p.Id.ToString() + "_0.jpg"
            }).ToList();
            return model;
        }

        /// <summary>
        /// Método que devuelve un instrumento por su id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public InstrumentViewModel GetById(int id)
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IInstrumentManager instrumentManager = new InstrumentManager(db);
            IUserManager userManager = new UserManager(db);
            var instrument = instrumentManager.GetById(id);
            var user = userManager.GetById(instrument.UserId);
            InstrumentViewModel model = new InstrumentViewModel()
            {
                Id = id,
                Name = instrument.Brand + " - " + instrument.Model,
                Brand = instrument.Brand,
                Price = instrument.Price,
                Description = instrument.Description,
                DateTime = instrument.DateTime,
                User = user,
                Image = "http://trademusic2.azurewebsites.net/images/instruments/" + id.ToString() + "_0.jpg",
                Image1 = "http://trademusic2.azurewebsites.net/images/instruments/" + id.ToString() + "_0.jpg",
                Image2 = "http://trademusic2.azurewebsites.net/images/instruments/" + id.ToString() + "_1.jpg",
                Image3 = "http://trademusic2.azurewebsites.net/images/instruments/" + id.ToString() + "_2.jpg",
                Image4 = "http://trademusic2.azurewebsites.net/images/instruments/" + id.ToString() + "_3.jpg"
            };
            return model;
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos de una familia
        /// </summary>
        /// <param name="familyId"></param>
        /// <returns></returns>
        public List<InstrumentViewModel> GetInstrumentsByFamily(int familyId)
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IInstrumentManager instrumentManager = new InstrumentManager(db);

            var model = instrumentManager.GetAllByFamily(familyId).Select(p => new InstrumentViewModel
            {
                Id = p.Id,
                Name = p.Brand + " - " + p.Model,
                Brand = p.Brand,
                Price = p.Price,
                Description = p.Description,
                Family = p.Family.Name,
                EngFamily = p.Family.EnglishName,
                DateTime = p.DateTime,
                User = p.User,
                Image = "http://trademusic2.azurewebsites.net/images/instruments/" + p.Id.ToString() + "_0.jpg"
            }).ToList();

            return model;
        }

        /// <summary>
        /// Método que devuelve todos los instrumentos por familia y ciudad
        /// </summary>
        /// <param name="familyId"></param>
        /// <param name="city"></param>
        /// <returns></returns>
        public List<InstrumentViewModel> GetInstrumentsByFamilyAndCity(int familyId, string city)
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IInstrumentManager instrumentManager = new InstrumentManager(db);

            var model = instrumentManager.GetAllByFamilyAndCity(familyId, city).Select(p => new InstrumentViewModel
            {
                Id = p.Id,
                Name = p.Brand + " - " + p.Model,
                Brand = p.Brand,
                Price = p.Price,
                Description = p.Description,
                Family = p.Family.Name,
                EngFamily = p.Family.EnglishName,
                DateTime = p.DateTime,
                User = p.User,
                Image = "http://trademusic2.azurewebsites.net/images/instruments/" + p.Id.ToString() + "_0.jpg"
            }).ToList();

            return model;
        }
    }
}
