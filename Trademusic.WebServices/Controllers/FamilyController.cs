﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Trademusic.Application;
using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;
using Trademusic.DAL;

namespace Trademusic.WebServices.Controllers
{
    public class FamilyController : ApiController
    {
        /// <summary>
        /// Método que devuelve todas las familias
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Family> GetFamilies()
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IFamilyManager familyManager = new FamilyManager(db);
            return familyManager.GetAll().Where(f => f.ParentId == 2 || f.ParentId == 3 || f.ParentId == 4).ToList();
        }

        /// <summary>
        /// Método que devuelve una familia por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Family GetById(int id)
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IFamilyManager familyManager = new FamilyManager(db);
            return familyManager.GetById(id);
        }
    }
}
