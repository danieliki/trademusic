﻿using System.Net.Http.Headers;
using System.Web.Http;

namespace Trademusic.WebServices
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("FamiliaApi", "api/families/{controller}/{familyId}", 
                defaults: new { controller= "instrument", action= "GetInstrumentsByFamily", familyId = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute("LoginApi", "api/login/{controller}/{email}/{pass}",
                defaults: new { controller = "user", action = "GetLoginUser", email = RouteParameter.Optional, pass = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute("MapApi", "api/map/{controller}/{familyId}/{city}",
               defaults: new { controller = "instrument", action = "GetInstrumentsByFamilyAndCity", familyId = RouteParameter.Optional, city = RouteParameter.Optional }
           );

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}
