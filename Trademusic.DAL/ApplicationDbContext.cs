﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Trademusic.CORE.Contracts;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.DAL
{

    /// <summary>
    /// Clase de contexto de datos de la aplicacion
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection", false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityRole>().HasKey(r => r.Id).Property(p => p.Name).IsRequired();
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });
            modelBuilder.Entity<IdentityUserLogin>().HasKey(u => new { u.UserId, u.LoginProvider, u.ProviderKey });
            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Colección de datos persistibles de instrumentos
        /// </summary>
        public DbSet<Instrument> Instruments { get; set; }

        /// <summary>
        /// Colección de datos persistibles de favoritos
        /// </summary>
        public DbSet<Favorite> Favorites { get; set; }

        /// <summary>
        /// Colección de datos persistibles de familias
        /// </summary>
        public DbSet<Family> Families { get; set; }

        /// <summary>
        /// Colección de datos persistibles de mensajes
        /// </summary>
        public DbSet<Message> Messages { get; set; }
    }
}