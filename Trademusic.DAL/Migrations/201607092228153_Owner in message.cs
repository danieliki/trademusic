namespace Trademusic.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Ownerinmessage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "OwnerId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "OwnerId");
        }
    }
}
