namespace Trademusic.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuitYearisSoldandChangedeInstrument : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Instruments", "Change");
            DropColumn("dbo.Instruments", "Year");
            DropColumn("dbo.Instruments", "IsSold");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Instruments", "IsSold", c => c.Boolean(nullable: false));
            AddColumn("dbo.Instruments", "Year", c => c.Int(nullable: false));
            AddColumn("dbo.Instruments", "Change", c => c.String());
        }
    }
}
