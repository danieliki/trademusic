namespace Trademusic.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReadinMessages : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "Read", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "Read");
        }
    }
}
