namespace Trademusic.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Start : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Families", "EnglishName", c => c.String(nullable: false));
            AddColumn("dbo.Messages", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Messages", "UserId");
            AddForeignKey("dbo.Messages", "UserId", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Messages", "UserName");
            DropColumn("dbo.Messages", "UserEmail");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Messages", "UserEmail", c => c.String());
            AddColumn("dbo.Messages", "UserName", c => c.String());
            DropForeignKey("dbo.Messages", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Messages", new[] { "UserId" });
            DropColumn("dbo.Messages", "UserId");
            DropColumn("dbo.Families", "EnglishName");
        }
    }
}
