﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;

namespace Trademusic.IFR
{
    /// <summary>
    /// Clase que genera la inyección de dependencias
    /// </summary>
    public class IoC
    {
        protected IUnityContainer container;

        #region Singleton
        private static readonly IoC current = new IoC();

        public static IoC Current
        {
            get
            {
                return current;
            }
        }
        #endregion

        #region metodos publicos
        public T Resolve<T>()
        {
            return container.Resolve<T>();
        }

        public T Resolve<T>(string name)
        {
            return container.Resolve<T>(name);
        }

        public object Resolve(Type t)
        {
            return container.Resolve(t);
        }

        public object Resolve(Type t, string name)
        {
            return container.Resolve(t, name);
        }

        public object ResolveAll<T>()
        {
            return container.ResolveAll<T>();
        }

        public IEnumerable<object> ResolveAll(Type type)
        {
            return container.ResolveAll(type);
        }
        #endregion



        #region Constructores
        static IoC()
        {
        }

        protected IoC()
        {
            container = new UnityContainer();
            container.RegisterType(
                GetType("Trademusic.CORE.Contracts.IApplicationDbContext, Trademusic.CORE"),
                GetType("Trademusic.DAL.ApplicationDbContext, Trademusic.DAL"),
                new ContainerControlledLifetimeManager());
            container.RegisterType(
                GetType("Trademusic.CORE.Contracts.IInstrumentManager, Trademusic.CORE"),
                GetType("Trademusic.Application.InstrumentManager, Trademusic.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("Trademusic.CORE.Contracts.IFamilyManager, Trademusic.CORE"),
                GetType("Trademusic.Application.FamilyManager, Trademusic.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("Trademusic.CORE.Contracts.IMessageManager, Trademusic.CORE"),
                GetType("Trademusic.Application.MessageManager, Trademusic.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("Trademusic.CORE.Contracts.IUserManager, Trademusic.CORE"),
                GetType("Trademusic.Application.UserManager, Trademusic.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("Trademusic.CORE.Contracts.IFavoriteManager, Trademusic.CORE"),
                GetType("Trademusic.Application.FavoriteManager, Trademusic.Application"),
                new TransientLifetimeManager());
        }

        private Type GetType(string typeString)
        {
            Type type = Type.GetType(typeString);
            if (type == null)
            {
                throw new Exception("El tipo no se ha podido resolver: " + typeString);
            }
            return type;
        }
        #endregion
    }
}