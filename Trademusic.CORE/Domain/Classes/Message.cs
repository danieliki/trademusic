﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Trademusic.CORE.Domain.Classes
{
    /// <summary>
    /// Clase de mensajes que el usuario puede hacer acerca de un instrumento
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Id del mensaje
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Texto del mensaje
        /// </summary>
        [Required(ErrorMessage = "El campo TEXTO es obligatorio.")]
        [MaxLength(250)]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }

        #region Usuario
        /// <summary>
        /// Identificador del usuario que ha escrito el mensaje
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }

        /// <summary>
        /// Usuario que ha escrito el mensaje
        /// </summary>
        public ApplicationUser User { get; set; }
        #endregion 

        /// <summary>
        /// Fecha del mensaje
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateTime { get; set; }

        #region Instrumento
        /// <summary>
        /// Id del instrumento sobre el que se ha escrito el mensaje
        /// </summary>
        [ForeignKey("Instrument")]
        public int InstrumentId { get; set; }

        /// <summary>
        /// Instrumento sobre el que se ha escrito el mensaje
        /// </summary>
        public Instrument Instrument { get; set; }
        #endregion

        /// <summary>
        /// Identificador del usuario al que pertenece el instrumento que recibe el mensaje
        /// </summary>
        public string OwnerId { get; set; }

        /// <summary>
        /// Campo que indica si el mensajelo ha leido el usuario receptor o no
        /// </summary>
        public bool Read { get; set; }
    }
}