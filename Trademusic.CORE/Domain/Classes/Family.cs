﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Trademusic.CORE.Domain.Classes
{
    /// <summary>
    /// Clase que representa el orden jerarquico de los instrumentos
    /// </summary>
    public class Family
    {
        /// <summary>
        /// Identificador de la familia
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Nombre de la familia
        /// </summary>
        [Required(ErrorMessage = "El campo NOMBRE es obligatorio.")]
        public string Name { get; set; }

        /// <summary>
        /// Nombre de la familia en inglés
        /// </summary>
        [Required(ErrorMessage = "El campo ENGLISHNAME es obligatorio.")]
        public string EnglishName { get; set; }

        /// <summary>
        /// Identificador de la familia padre
        /// </summary>
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }

        /// <summary>
        /// Familia padre a la que pertenece
        /// </summary>
        public Family Parent { get; set; }
    }
}