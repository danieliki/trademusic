﻿namespace Trademusic.CORE.Domain.Classes
{
    public class Favorite
    {
        /// <summary>
        /// Identificador del favorito
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Instrumento que se convierte en favorito
        /// </summary>
        public Instrument Instrument { get; set; }

        /// <summary>
        /// Usuario que convierte el instrumento en favorito
        /// </summary>
        public ApplicationUser User { get; set; } 
    }
}