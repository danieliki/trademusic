﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Trademusic.CORE.Domain.Classes
{
    public class Instrument
    {
        /// <summary>
        /// Identificador del instrumento
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Identificador del usuario a la que pertenece el instrumento
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }

        /// <summary>
        /// Usuario al que pertenece el instrumento
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Marca del instrumento
        /// </summary>
        [Required(ErrorMessage = "El campo MARCA es obligatorio.")]
        public string Brand { get; set; }

        /// <summary>
        /// Modelo del instrumento
        /// </summary>
        [Required(ErrorMessage = "El campo MODELO es obligatorio.")]
        public string Model { get; set; }

        /// <summary>
        /// Nombre completo del instrumento
        /// </summary>
        public string FullName
        {
            get
            {
                return Brand + " - " + Model;
            }
        }

        /// <summary>
        /// Precio por el que se vende el instrumento
        /// </summary>
        [Required(ErrorMessage = "El campo PRECIO es obligatorio.")]
        public int Price { get; set; }

        /// <summary>
        /// Descripción del instrumento
        /// </summary>
        [Required(ErrorMessage = "El campo DESCRIPCIÓN es obligatorio.")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        /// <summary>
        /// Fecha del creación del anuncio de venta del instrumento
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateTime { get; set; }

        #region Familia
        /// <summary>
        /// Identificador de la familia a la que pertenece el instrumento
        /// </summary>
        [Required(ErrorMessage = "El campo FAMILIA es obligatorio.")]
        [ForeignKey("Family")]
        public int FamilyId { get; set; }

        /// <summary>
        /// Familia del instrumento
        /// </summary>
        public Family Family { get; set; }
        #endregion

        /// <summary>
        /// Colección de mensajes del instrumento
        /// </summary>
        public List<Message> Messages { get; set; }        
    }
}