using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity.EntityFramework;

using Trademusic.CORE.Domain.Classes;

namespace Trademusic.CORE.Contracts
{
    /// <summary>
    /// Interfaz del contexto de BBDD
    /// </summary>
    public interface IApplicationDbContext
    {
        DbSet<Message> Messages { get; set; }

        DbSet<Family> Families { get; set; }

        DbSet<Instrument> Instruments { get; set; }

        DbSet<Favorite> Favorites { get; set; }

        IDbSet<ApplicationUser> Users { get; set; }

        IDbSet<IdentityRole> Roles { get; set; }

        bool RequireUniqueEmail { get; set; }

        Database Database { get; }

        DbChangeTracker ChangeTracker { get; }

        DbContextConfiguration Configuration { get; }

        DbSet Set(Type entityType);

        int SaveChanges();

        Task<int> SaveChangesAsync();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        IEnumerable<DbEntityValidationResult> GetValidationErrors();

        DbEntityEntry Entry(object entity);

        void Dispose();

        string ToString();

        bool Equals(object obj);

        int GetHashCode();

        Type GetType();

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
    }
}