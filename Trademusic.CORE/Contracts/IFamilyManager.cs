﻿using System.Linq;

using Trademusic.CORE.Domain.Classes;

namespace Trademusic.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de las familias de los instrumentos
    /// </summary>
    public interface IFamilyManager : IManager<Family>
    {
        Family GetByParentId(int parentId);

        IQueryable<Family> GetAllByParentId(int parentId);
    }
}