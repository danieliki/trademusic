﻿using Trademusic.CORE.Domain.Classes;

namespace Trademusic.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de los usuarios
    /// </summary>
    public interface IUserManager : IManager<ApplicationUser>
    {
        ApplicationUser GetLoginUser(string email, string pass);
    }
}