﻿using System.Linq;

using Trademusic.CORE.Domain.Classes;

namespace Trademusic.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de los instrumentos favoritos
    /// </summary>
    public interface IFavoriteManager : IManager<Favorite>
    {
        IQueryable<Favorite> GetAllByUserId(string id);
    }
}