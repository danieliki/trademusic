﻿using System.Linq;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de los instrumentos
    /// </summary>
    public interface IInstrumentManager : IManager<Instrument>
    {
        IQueryable<Instrument> GetAllByBrand(string brand);

        IQueryable<Instrument> GetAllByFamily(string familyName);

        IQueryable<Instrument> GetAllByFamily(int familyId);

        IQueryable<Instrument> GetAllByPriceRange(decimal price1, decimal price2);

        IQueryable<Instrument> GetAllByUserCity(string city);

        IQueryable<Instrument> GetAllByUserId(string id);

        Instrument GetByUserId(string id);

        IQueryable<Instrument> GetAllByFamilyAndBrand(int family, string brand);

        IQueryable<Instrument> GetAllByFamilyAndCity(int family, string city);

        IQueryable<Instrument> GetAllByFamilyAndPriceRange(int family, int price1, int price2);
    }
}