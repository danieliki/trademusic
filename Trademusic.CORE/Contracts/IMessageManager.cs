﻿using System.Linq;
using Trademusic.CORE.Domain.Classes;

namespace Trademusic.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de los mensajes de un instrumento
    /// </summary>
    public interface IMessageManager : IManager<Message>
    {
        IQueryable<Message> GetAllByInstrumentId(int instrumentId);

        IQueryable<Message> GetAllByOwnerId(string ownerId);

        IQueryable<Message> GetAllByOwnerIdNoRead(string ownerId);
    }
}